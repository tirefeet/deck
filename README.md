# Cards Deck

A simple program meant to serve as an introduction to Elixir.

The program generates a set of playing cards and deals them.

## Topics Covered

- Immutability
- List comprehension
- Recursion
- Combining elements of multiple arrays
- Tuples
- Accessing elements of a list through pattern matching
- Invoking Erlang
- File operations with pattern matching
- Variable assignment with pattern matching
- Understanding binary files
- Error handling
- Atoms
- Hard-coded pattern matching
- `case` statement
- Piping
- Dependencies
- Generating documentation
- Running unit and case tests
- Maps
- Using pattern matching for assigning variables in maps
- Adding properties to maps
- Keyword lists (mixture of lists and tuples, useful for database queries)
- Key duplication (keyword lists vs maps)

## How to run 

Make sure Elixir is installed on your system. You can do this from command line with:

`elixir -v`

Go to project directory and install dependencies:

`mix deps.get`

Run the project from interactive shell:

`iex -S mix`
